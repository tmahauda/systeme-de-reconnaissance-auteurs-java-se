package authorReco;

import langModel.*;



/**
 * Class CreateLanguageModels: a class to create the language models used in the recognition systems.
 * 
 * @author N. Hernandez and S. Quiniou (2017)
 *
 */
public class CreateLanguageModels {

	/**
	 * Main method.
	 * 
	 * @param args arguments of the main method.
	 */
	public static void main(String[] args) {
		
		VocabularyInterface vocabulary;
		NgramCountsInterface ngramCounts;
		
		vocabulary = new Vocabulary();
		vocabulary.readVocabularyFile("lm/author_corpus/corpus_20000.vocab");
				
		//Création des modèles de langue en unigramme pour chaque auteur
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/balzac.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_balzac.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/hugo.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_hugo.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/maupassant.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_maupassant.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/moliere.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_moliere.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/montaigne.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_montaigne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/sand.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_sand.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tocqueville.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_tocqueville.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tolstoi.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_tolstoi.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/verne.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_verne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/zola.txt", vocabulary, 1);
		ngramCounts.writeNgramCountFile("lm/author_corpus/unigram_zola.lm");

		//Création des modèles de langue en bigramme pour chaque auteur
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/balzac.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_balzac.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/hugo.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_hugo.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/maupassant.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_maupassant.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/moliere.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_moliere.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/montaigne.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_montaigne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/sand.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_sand.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tocqueville.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_tocqueville.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tolstoi.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_tolstoi.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/verne.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_verne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/zola.txt", vocabulary, 2);
		ngramCounts.writeNgramCountFile("lm/author_corpus/bigram_zola.lm");
		
		
		//Création des modèles de langue en trigramme pour chaque auteur
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/balzac.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_balzac.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/hugo.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_hugo.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/maupassant.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_maupassant.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/moliere.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_moliere.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/montaigne.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_montaigne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/sand.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_sand.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tocqueville.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_tocqueville.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tolstoi.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_tolstoi.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/verne.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_verne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/zola.txt", vocabulary, 3);
		ngramCounts.writeNgramCountFile("lm/author_corpus/trigram_zola.lm");
		
		//Création des modèles de langue en quadrigramme pour chaque auteur
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/balzac.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_balzac.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/hugo.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_hugo.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/maupassant.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_maupassant.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/moliere.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_moliere.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/montaigne.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_montaigne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/sand.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_sand.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tocqueville.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_tocqueville.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/tolstoi.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_tolstoi.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/verne.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_verne.lm");
		
		ngramCounts = new NgramCounts();
		ngramCounts.scanTextFile("data/author_corpus/train/zola.txt", vocabulary, 4);
		ngramCounts.writeNgramCountFile("lm/author_corpus/quadrigram_zola.lm");


	}

}
