package authorReco;


import java.util.*;

import authorEval.*;
import langModel.*;


/**
 * Class AuthorRecognizer1: a first author recognition system that recognizes 
 * the author of a sentence by using the language models read from a configuration system.
 * (no unknown author can be detected)
 * 
 * @author N. Hernandez and S. Quiniou (2017)
 *
 */
public class AuthorRecognizer1 extends AuthorRecognizerAbstractClass {
	/**
	 * Map of LanguageModels associated with each author (each author can be 
	 * associated with several language models). 
	 * The keys to the first map are the names of the authors (e.g., "zola"), the keys 
	 * to the second map are the names associated with each file containing a language model 
	 * (e.g., "zola-bigrams"), and the values of the second map are the LanguageModel objects 
	 * built from the file path given in the AuthorConfigurationFile attribute.
	 */
	protected Map<String, Map<String, LanguageModelInterface>> authorLangModelsMap;
	
	/**
	 * Constructor.
	 * 
	 * @param configFile the file path of the configuration file containing the information 
	 * on the language models (author, name and file path).
	 * @param vocabFile the file path of the file containing the common vocabulary
	 * for all the language models used in the recognition system. 
	 * @param authorFile the file path of the file containing 
	 * the names of the authors recognized by the system.
	 */
	public AuthorRecognizer1(String configFile, String vocabFile, String authorFile) {
		super();
		super.loadAuthorConfigurationFile(configFile);
		super.loadVocabularyFile(vocabFile);
		super.loadAuthorFile(authorFile);

		this.authorLangModelsMap = new HashMap<String, Map<String, LanguageModelInterface>>();
		
		LanguageModelInterface langModel = new LaplaceLanguageModel();
		NgramCountsInterface ngramCounts = new NgramCounts();
		Map<String, LanguageModelInterface> langModelAuthor = new HashMap<String, LanguageModelInterface>();
		
		//Pour chaque auteur
		for(Iterator<String> it = super.configLangModels.getAuthors().iterator() ; it.hasNext() ; )
		{
			String author = it.next();
	
				//Pour chacun des modèles de langue de l'auteur
				for(Iterator<String> it2 = super.configLangModels.getNgramCountNames(author).iterator() ; it2.hasNext() ; )
				{
					langModel = new LaplaceLanguageModel();
					ngramCounts = new NgramCounts();
					
					String ngramCountName = it2.next();	
					String ngramCountPath = super.configLangModels.getNgramCountPath(author, ngramCountName);
					
					//On récupère les modèles de langue générés par la classe CreateLanguageModels pour les associer à chaque auteur
					
					ngramCounts.readNgramCountsFile(ngramCountPath);
					langModel.setNgramCounts(ngramCounts, super.getVocabularyLM());
					
					if(!this.authorLangModelsMap.containsKey(author))
					{
						langModelAuthor = new HashMap<String, LanguageModelInterface>();
					}
					else langModelAuthor = this.authorLangModelsMap.get(author);
					
					langModelAuthor.put(ngramCountName, langModel);
					
					this.authorLangModelsMap.put(author, langModelAuthor);	
				}
		}
	}
	
	
	
	/**
	 * Method recognizing and returning the author of the given sentence 
	 * (the unknown author can also be picked up).
	 * 
	 * @param sentence the sentence whose author is to recognize.
	 * @return the author of the sentence as recognized by the recognition system.
	 */
	public String recognizeAuthorSentence(String sentence) {
		
		String recognizeAuthor = UNKNOWN_AUTHOR;
		double proba = 0;
		double probaMax = -1;
		double probaMin = 1;
		
		//Pour chaque auteur
		for(Iterator<String> it = this.authorLangModelsMap.keySet().iterator() ; it.hasNext() ; )
		{
			String author = it.next();
			
			//Avec le modèle de langue construit en unigramme
			proba = this.authorLangModelsMap.get(author).get(author+"_uni").getSentenceProb(sentence);
			
			//On récupère le nom de l'auteur reconnu ayant la probabilité la plus haute
			if(proba > probaMax)
			{
				probaMax = proba;
				recognizeAuthor = author;
			}
			//On récupère la probabilité la plus faible de la phrase selon chaque modele de langue des auteurs pour ensuite les comparer sur l'ensemble des phrases
			//et determiner quelle est la probabilité la plus faible pour l'utiliser dans la classe UnknownAuthorRecognizer1. Elle jouera le rôle de condition
			//pour dire a quel moment un auteur est inconnu
			else if (proba < probaMin)
			{
				probaMin = proba;
			}
			
			proba = 0;
		}
		
		//On écrit la probabilité la plus faible trouvé dans la phrase avec unigramme
		MiscUtils.writeFile(probaMin+"\n", "data/author_corpus/validation/probaMinUnigramme.txt", true);

		return recognizeAuthor;
	}
	
	
	
	/**
	 * Main method.
	 * 
	 * @param args arguments of the main method.
	 */
	public static void main(String[] args) {
		
		//initialization of the recognition system 1

		AuthorRecognizer1 authorRecognizer1 = new AuthorRecognizer1("lm/author_corpus/fichConfig_ngram_sentences.txt","lm/author_corpus/corpus_20000.vocab","data/author_corpus/validation/authors.txt");
		
		//computation of the hypothesis author file
				
		authorRecognizer1.recognizeFileLanguage("data/author_corpus/validation/sentences.txt", "data/author_corpus/validation/authors_hyp10.txt");

		//computation of the performance of the recognition system 1
				
		System.out.println(RecognizerPerformance.evaluateAuthors("data/author_corpus/validation/authors_ref.txt", "data/author_corpus/validation/authors_hyp10.txt"));

		//initialization of the recognition system test 1

		AuthorRecognizer1 authorRecognizerTest1 = new AuthorRecognizer1("lm/author_corpus/fichConfig_ngram_sentences.txt","lm/author_corpus/corpus_20000.vocab","data/author_corpus/validation/authors.txt");
				
		//computation of the hypothesis author file
				
		authorRecognizerTest1.recognizeFileLanguage("data/author_corpus/test/sentences.txt", "data/author_corpus/test/authors_hyp1.txt");

	}
}
