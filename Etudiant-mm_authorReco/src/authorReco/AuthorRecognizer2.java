package authorReco;


import java.util.*;

import authorEval.*;
import langModel.*;


/**
 * Class AuthorRecognizer1: a first author recognition system that recognizes 
 * the author of a sentence by using the language models read from a configuration system.
 * (no unknown author can be detected)
 * 
 * @author N. Hernandez and S. Quiniou (2017)
 *
 */
public class AuthorRecognizer2 extends AuthorRecognizer1 {
	
	/**
	 * Constructor.
	 * 
	 * @param configFile the file path of the configuration file containing the information 
	 * on the language models (author, name and file path).
	 * @param vocabFile the file path of the file containing the common vocabulary
	 * for all the language models used in the recognition system. 
	 * @param authorFile the file path of the file containing 
	 * the names of the authors recognized by the system.
	 */
	public AuthorRecognizer2(String configFile, String vocabFile, String authorFile) {
		super(configFile,vocabFile,authorFile);
	}
	
	
	
	/**
	 * Method recognizing and returning the author of the given sentence 
	 * (the unknown author can also be picked up).
	 * 
	 * @param sentence the sentence whose author is to recognize.
	 * @return the author of the sentence as recognized by the recognition system.
	 */
	public String recognizeAuthorSentence(String sentence) {
		
		String recognizeAuthor = UNKNOWN_AUTHOR;
		double proba = 0;
		double probaMax = -1;
		double probaMin = 1;
		
		//Pour chaque auteur
		for(Iterator<String> it = super.authorLangModelsMap.keySet().iterator() ; it.hasNext() ; )
		{
			String author = it.next();
			
			//Avec le modèle de langue construit en bigramme
			proba = super.authorLangModelsMap.get(author).get(author+"_bi").getSentenceProb(sentence);
			
			//On récupère le nom de l'auteur reconnu ayant la probabilité la plus haute
			if(proba > probaMax)
			{
				probaMax = proba;
				recognizeAuthor = author;
			}
			//On récupère la probabilité la plus faible de la phrase selon chaque modele de langue des auteurs pour ensuite les comparer sur l'ensemble des phrases
			//et determiner quelle est la probabilité la plus faible pour l'utiliser dans la classe UnknownAuthorRecognizer1. Elle jouera le rôle de condition
			//pour dire a quel moment un auteur est inconnu
			else if (proba < probaMin)
			{
				probaMin = proba;
			}
			
			proba = 0;
		}
		
		//On écrit la probabilité la plus faible trouvé dans la phrase
		MiscUtils.writeFile(probaMin+"\n", "data/author_corpus/validation/probaMinBigramme.txt", true);
		
		return recognizeAuthor;
	}
	
	
	
	/**
	 * Main method.
	 * 
	 * @param args arguments of the main method.
	 */
	public static void main(String[] args) {
		
		//initialization of the recognition system 100 sentences

		AuthorRecognizer2 authorRecognizer100Sentences = new AuthorRecognizer2("lm/small_author_corpus/fichConfig_bigram_1000sentences.txt","lm/small_author_corpus/corpus_20000.vocab","data/small_author_corpus/validation/authors.txt");
		
		//computation of the hypothesis author file
				
		authorRecognizer100Sentences.recognizeFileLanguage("data/small_author_corpus/validation/sentences_100sentences.txt", "data/small_author_corpus/validation/authors_100sentences_hyp1.txt");

		//computation of the performance of the recognition system 100 sentences
				
		System.out.println(RecognizerPerformance.evaluateAuthors("data/small_author_corpus/validation/authors_100sentences_ref.txt", "data/small_author_corpus/validation/authors_100sentences_hyp1.txt"));
		
		//initialization of the recognition system 2

		AuthorRecognizer2 authorRecognizer2 = new AuthorRecognizer2("lm/author_corpus/fichConfig_ngram_sentences.txt","lm/author_corpus/corpus_20000.vocab","data/author_corpus/validation/authors.txt");
				
		//computation of the hypothesis author file
				
		authorRecognizer2.recognizeFileLanguage("data/author_corpus/validation/sentences.txt", "data/author_corpus/validation/authors_hyp2.txt");

		//computation of the performance of the recognition system 2
				
		System.out.println(RecognizerPerformance.evaluateAuthors("data/author_corpus/validation/authors_ref.txt", "data/author_corpus/validation/authors_hyp2.txt"));

		//initialization of the recognition system test 2

		AuthorRecognizer2 authorRecognizerTest2 = new AuthorRecognizer2("lm/author_corpus/fichConfig_ngram_sentences.txt","lm/author_corpus/corpus_20000.vocab","data/author_corpus/validation/authors.txt");
				
		//computation of the hypothesis author file
				
		authorRecognizerTest2.recognizeFileLanguage("data/author_corpus/test/sentences.txt", "data/author_corpus/test/authors_hyp2.txt");

	}
}
