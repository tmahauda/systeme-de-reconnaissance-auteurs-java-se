package authorReco;


import java.util.*;

import authorEval.*;
import langModel.*;


/**
 * Class UnknownAuthorRecognizer1: a first author recognition system that recognizes 
 * the author of a sentence by using the language models read from a configuration system.
 * (unknown authors can be detected)
 * 
 * @author N. Hernandez and S. Quiniou (2017)
 *
 */
public class UnknownAuthorRecognizer3 extends AuthorRecognizer1 {

	/**
	 * Constructor.
	 * 
	 * @param configFile the file path of the configuration file containing the information 
	 * on the language models (author, name and file path).
	 * @param vocabFile the file path of the file containing the common vocabulary
	 * for all the language models used in the recognition system. 
	 * @param authorFile the file path of the file containing 
	 * the names of the authors recognized by the system.
	 */
	public UnknownAuthorRecognizer3(String configFile, String vocabFile, String authorFile) {
		super(configFile, vocabFile, authorFile);
	}

	
	
	/**
	 * Method recognizing and returning the author of the given sentence 
	 * (the unknown author can also be picked up).
	 * 
	 * @param sentence the sentence whose author is to recognize.
	 * @return the author of the sentence as recognized by the recognition system.
	 */
	public String recognizeAuthorSentence(String sentence) {
		
		String recognizeAuthor = UNKNOWN_AUTHOR;
		double proba = 0;
		double probaMax = -1;
		double probaMin = 1;
		double probaMinCourante = 0;
		List<String> probaMins = new ArrayList<String>();
		
		probaMins = MiscUtils.readTextFileAsStringList("data/author_corpus/validation/probaMinTrigramme.txt");
		
		for(String prob : probaMins)
		{
			probaMinCourante = Double.parseDouble(prob);
			
			if(probaMinCourante < probaMin && probaMinCourante != 0.0)
			{
				probaMin = probaMinCourante;
			}
		}
		
		//Pour chaque auteur
		for(Iterator<String> it = super.authorLangModelsMap.keySet().iterator() ; it.hasNext() ; )
		{
			String author = it.next();
			
			//Avec le modèle de langue construit en unigramme
			proba = this.authorLangModelsMap.get(author).get(author+"_tri").getSentenceProb(sentence);
			
			//Si la probabilité courante est plus grande que la probabilité minimal 
			//On récupère le nom de l'auteur reconnu ayant la probabilité la plus haute
			if(proba > probaMax && proba > probaMin)
			{
				probaMax = proba;
				recognizeAuthor = author;
			}
			
			proba = 0;
		}
		
		
		return recognizeAuthor;

	}

	
	
	/**
	 * Main method.
	 * 
	 * @param args arguments of the main method.
	 */
	public static void main(String[] args) {
		
		//initialization of the recognition system unknown 3

		UnknownAuthorRecognizer3 unknownAuthorRecognizer3 = new UnknownAuthorRecognizer3("lm/author_corpus/fichConfig_ngram_sentences.txt","lm/author_corpus/corpus_20000.vocab","data/author_corpus/validation/authors.txt");
						
		//computation of the hypothesis author file
						
		unknownAuthorRecognizer3.recognizeFileLanguage("data/author_corpus/validation/sentences.txt", "data/author_corpus/validation/authors_hyp6.txt");

		//computation of the performance of the recognition system unknown 3
						
		System.out.println(RecognizerPerformance.evaluateAuthors("data/author_corpus/validation/authors_ref.txt", "data/author_corpus/validation/authors_hyp6.txt"));
		
		//initialization of the recognition system unknown test 1

		UnknownAuthorRecognizer3 unknownAuthorRecognizerTest3 = new UnknownAuthorRecognizer3("lm/author_corpus/fichConfig_ngram_sentences.txt","lm/author_corpus/corpus_20000.vocab","data/author_corpus/validation/authors.txt");
				
		//computation of the hypothesis author file
				
		unknownAuthorRecognizerTest3.recognizeFileLanguage("data/author_corpus/test/sentences.txt", "data/author_corpus/test/authors_hyp6.txt");

	}

}
