package langModel;

import java.util.Iterator;

/**
 * Class LaplaceLanguageModel: class inheriting the class NaiveLanguageModel by creating 
 * a n-gram language model using a Laplace smoothing.
 * 
 * @author ... (2017)
 *
 */
public class LaplaceLanguageModel extends NaiveLanguageModel {

	public LaplaceLanguageModel()
	{
		super();
	}
	
	
	@Override
	public Double getNgramProb(String ngram) {
		double nominateur = 0;
		double denominateur = 0;
		String history = "";
		
		nominateur = super.ngramCounts.getCounts(ngram) + 1;
		history = NgramUtils.getHistory(ngram, super.getLMOrder());
		
		for(Iterator<String> ngramListe = super.ngramCounts.getNgrams().iterator(); ngramListe.hasNext() ; )
		{
			
			if(history.equals(NgramUtils.getHistory(ngramListe.next(), super.getLMOrder())))
			{
				denominateur++;
			}
		}
		
		denominateur += super.vocabulary.getSize();
		
		return nominateur/denominateur;
	}
}
