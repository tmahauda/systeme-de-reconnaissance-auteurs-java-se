package langModel;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * Class Vocabulary: class implementing the interface VocabularyInterface.
 * 
 * @author ... (2017)
 *
 */
public class Vocabulary implements VocabularyInterface {
	/**
	 * The set of words corresponding to the vocabulary.
	 */
	protected Set<String> vocabulary;

	
	/**
	 * Constructor.
	 */
	public Vocabulary(){
		this.vocabulary = new HashSet<String>();
	}
	
	
	@Override
	public int getSize() {
		return this.vocabulary.size();
	}

	@Override
	public Set<String> getWords() {
		return this.vocabulary;
	}

	@Override
	public boolean contains(String word) {
		return this.vocabulary.contains(word);
	}

	@Override
	public void addWord(String word) {
		this.vocabulary.add(word);
	}

	@Override
	public void removeWord(String word) {
		this.vocabulary.remove(word);
	}

	@Override
	public void scanNgramSet(Set<String> ngramSet) {
		this.vocabulary = ngramSet;
	}

	@Override
	public void readVocabularyFile(String filePath) {
		this.vocabulary = new HashSet<String>(MiscUtils.readTextFileAsStringList(filePath));
	}

	@Override
	public void writeVocabularyFile(String filePath) {
		String words = "";
		for(Iterator<String> word = this.vocabulary.iterator() ; word.hasNext() ; )
		{
			words = words + word.next() + "\n";
		}
		MiscUtils.writeFile(words, filePath, true);
	}

}
