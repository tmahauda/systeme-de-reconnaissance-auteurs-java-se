package langModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class NaiveLanguageModel: class implementing the interface LanguageModelInterface by creating a naive language model,
 * i.e. a n-gram language model with no smoothing.
 * 
 * @author ... (2017)
 *
 */
public class NaiveLanguageModel implements LanguageModelInterface {
	/**
	 * The NgramCountsInterface corresponding to the language model.
	 */
	protected NgramCountsInterface ngramCounts;
	
	/**
	 * The vocabulary of the language model.
	 */
	protected VocabularyInterface vocabulary;
	
	
	/**
	 * Constructor.
	 */
	public NaiveLanguageModel(){
		this.ngramCounts = new NgramCounts();
		this.vocabulary = new Vocabulary();
	}
	

	@Override
	public int getLMOrder() {
		return ngramCounts.getMaximalOrder();
	}

	@Override
	public void setNgramCounts(NgramCountsInterface ngramCounts, VocabularyInterface vocab) {
		this.ngramCounts=ngramCounts;
		this.vocabulary=vocab;
	}

	@Override
	public Double getNgramProb(String ngram) {
		double nominateur = 0.0;
		double denominateur = 0.0;
		String history = "";
		
		nominateur = this.ngramCounts.getCounts(ngram);
		history = NgramUtils.getHistory(ngram, this.getLMOrder());
		
		for(Iterator<String> ngramListe = this.ngramCounts.getNgrams().iterator(); ngramListe.hasNext() ; )
		{
			
			if(history.equals(NgramUtils.getHistory(ngramListe.next(), this.getLMOrder())))
			{
				denominateur++;
			}
		}
		
		if(denominateur==0.0)
		{
			return 0.0;
		}
		else return nominateur/denominateur;

	}

	@Override
	public Double getSentenceProb(String sentence) {
		double probaPhrase = 1;
		List<String> ngramListe = new ArrayList<String>();
		ngramListe = NgramUtils.decomposeIntoNgrams(sentence, this.getLMOrder());
		
		for(Iterator<String> ngram = ngramListe.iterator() ; ngram.hasNext() ; )
		{
			probaPhrase = probaPhrase * this.getNgramProb(ngram.next());
		}
		
		return probaPhrase;
	}

}
