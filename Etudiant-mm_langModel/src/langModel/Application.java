package langModel;

public class Application {

	public static void main(String[] args) {
		System.out.println(NgramUtils.getHistory("l' historique de cette phrase", 3));
		System.out.println(NgramUtils.generateNgrams("a b c d e f g", 1, 3).toString());
		System.out.println(NgramUtils.decomposeIntoNgrams("a b c d e f g", 3));
		
		Vocabulary vocab = new Vocabulary();
		vocab.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");
		
		NgramCounts ngramcounts = new NgramCounts();
		ngramcounts.scanTextFile("data/small_corpus/corpus_fr_train.txt", vocab, 2);
		
		NaiveLanguageModel languageModel = new NaiveLanguageModel();
		languageModel.setNgramCounts(ngramcounts, vocab);
		languageModel.getNgramProb("<s> antoine");
	}

}
