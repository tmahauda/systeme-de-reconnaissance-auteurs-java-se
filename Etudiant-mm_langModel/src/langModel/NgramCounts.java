package langModel;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


/**
 * Class NgramCounts: class implementing the interface NgramCountsInterface. 
 * 
 * @author N. Hernandez and S. Quiniou (2017)
 *
 */
public class NgramCounts implements NgramCountsInterface {
	/**
	 * The maximal order of the n-gram counts.
	 */
	protected int order;

	/**
	 * The map containing the counts of each n-gram.
	 */
	protected Map<String,Integer> ngramCounts;

	/**
	 * 
	 * The total number of words in the corpus.
	 * In practice, the total number of words will be increased when parsing a corpus 
	 * or when parsing a NgramCountsInterface file (only if the ngram encountered is a unigram one).
	 */
	protected int nbWordsTotal;
	
	
	/**
	 * Constructor.
	 */
	public NgramCounts(){
		this.order = 0;
		this.ngramCounts = new HashMap<String,Integer>();
		this.nbWordsTotal = 0;
	}
	

	/**
	 * Setter of the maximal order of the ngrams considered.
	 * 
	 * In practice, the method will be called when parsing the training corpus, 
	 * or when parsing the NgramCountsInterface file (using the maximal n-gram length encountered).
	 * 
	 * @param order the maximal order of n-grams considered.
	 */
	private void setMaximalOrder(int order) {
		this.order=order;
	}

	
	@Override
	public int getMaximalOrder() {
		return this.order;
	}

	
	@Override
	public int getNgramCounterSize() {	
		return this.ngramCounts.size();
	}

	
	@Override
	public int getTotalWordNumber(){
		for(Iterator<Integer> number = this.ngramCounts.values().iterator() ; number.hasNext() ;)
		{
			this.nbWordsTotal = this.nbWordsTotal + number.next();
		}
		return this.nbWordsTotal;
	}
	
	
	@Override
	public Set<String> getNgrams() {
		return this.ngramCounts.keySet();
	}

	
	@Override
	public int getCounts(String ngram) {
		if(this.ngramCounts.containsKey(ngram))
		{
			return this.ngramCounts.get(ngram);
		}
		return 0;
	}
	

	@Override
	public void incCounts(String ngram) {
		//Si le ngram n'existe pas dans la liste, alors on l'ajoute avec une occurrence à 1
		if(!this.ngramCounts.containsKey(ngram)) 
		{
			this.ngramCounts.put(ngram, 1);
		} 
		else 
		{
			int occurence = this.ngramCounts.get(ngram);
			occurence++;
			this.ngramCounts.put(ngram, occurence);
			// put remplace l'ancienne valeur si il en existait deja une
		}
	}

	
	@Override
	public void setCounts(String ngram, int counts) {
		this.ngramCounts.put(ngram, counts);
	}


	@Override
	public void scanTextFile(String filePath, VocabularyInterface vocab, int maximalOrder) {
		List<String> phrases = new ArrayList<String>();
		List<String> phrasesOOV = new ArrayList<String>();
		List<String> ngrams = new ArrayList<String>();
		phrases = MiscUtils.readTextFileAsStringList(filePath);
		
		//On vérifie si les mots des phrases sont présents dans le vocabulaire
		for(Iterator<String> phrase = phrases.iterator() ; phrase.hasNext() ; )
		{
		  phrasesOOV.add(NgramUtils.getStringOOV(phrase.next(), vocab));
		}
		
		//On génére les n-grams par maximalOrder des phrasesOOV
		for(Iterator<String> phraseOOV = phrasesOOV.iterator() ; phraseOOV.hasNext() ; )
		{
		  ngrams = NgramUtils.generateNgrams(phraseOOV.next(), 1, maximalOrder);
		  
		  for(Iterator<String> ngram = ngrams.iterator() ; ngram.hasNext() ;)
		  {
			  this.incCounts(ngram.next());
		  }
		}
		
		this.setMaximalOrder(maximalOrder);
	}

	
	@Override
	public void writeNgramCountFile(String filePath) {
		String ngramsCounts = "";
		Entry<String, Integer> ngramCountsCourant;
		for(Iterator<Entry<String,Integer>> ngramCounts = this.ngramCounts.entrySet().iterator() ; ngramCounts.hasNext() ; )
		{
			ngramCountsCourant = ngramCounts.next();
			ngramsCounts = ngramsCounts + ngramCountsCourant.getKey() + "\t" + ngramCountsCourant.getValue() + "\n";
		}
		MiscUtils.writeFile(ngramsCounts, filePath, true);
	}

	
	@Override
	public void readNgramCountsFile(String filePath) {
		String[] ngramsCount;
		int order = 0;
		List<String> words = MiscUtils.readTextFileAsStringList(filePath);
		
		for(Iterator<String> word = words.iterator() ; word.hasNext() ; )
		{
			String ngramCount = word.next();
			ngramsCount = ngramCount.split("\t");
			this.setCounts(ngramsCount[0], Integer.parseInt(ngramsCount[1]));
			if(ngramsCount[0].split("\\s+").length > order)
			{
				order = ngramsCount[0].split("\\s+").length;
			}
		}
		this.setMaximalOrder(order);
	}
	

}
