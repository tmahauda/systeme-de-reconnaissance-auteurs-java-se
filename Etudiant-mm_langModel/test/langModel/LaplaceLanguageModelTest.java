package langModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LaplaceLanguageModelTest {
	
	private LaplaceLanguageModel laplaceLanguageModel;

	@Before
	public void setUp() throws Exception {
		this.laplaceLanguageModel = new LaplaceLanguageModel();
	}

	@Test
	public void testGetNgramProb() {
		
		Vocabulary vocabulary = new Vocabulary();
		NgramCounts ngramCounts = new NgramCounts();
		
		vocabulary.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");
		ngramCounts.scanTextFile("data/small_corpus/corpus_fr_train.txt", vocabulary, 2);
		this.laplaceLanguageModel.setNgramCounts(ngramCounts, vocabulary);
		assertEquals(0.153, this.laplaceLanguageModel.getNgramProb("antoine écoute"), 0.001);
	}
	
	@Test
	public void testGetSentenceProb() {
		
		Vocabulary vocabulary = new Vocabulary();
		NgramCounts ngramCounts = new NgramCounts();
		
		vocabulary.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");
		ngramCounts.scanTextFile("data/small_corpus/corpus_fr_train.txt", vocabulary, 2);
		this.laplaceLanguageModel.setNgramCounts(ngramCounts, vocabulary);
		assertEquals(1.495E-5, this.laplaceLanguageModel.getSentenceProb("<s> antoine écoute une chanson </s>"), 0.001E-5);
	}

}
