package langModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NgramCountsTest {
	
	private NgramCounts ngramCounts;

	@Before
	public void setUp() throws Exception {
		this.ngramCounts = new NgramCounts();
	}

	@Test
	public void testNgramCounts() {
		assertEquals(0,this.ngramCounts.order);
		assertNotNull(this.ngramCounts.ngramCounts); 
		assertEquals(0,this.ngramCounts.nbWordsTotal);
	}

	@Test
	public void testGetMaximalOrder() {
		this.ngramCounts.order=3;
		assertEquals(3, this.ngramCounts.getMaximalOrder());
	}

	@Test
	public void testGetNgramCounterSize() {
		this.ngramCounts.ngramCounts.put("toto", 2);
		this.ngramCounts.ngramCounts.put("titi", 6);
		this.ngramCounts.ngramCounts.put("tata", 4);
		assertEquals(3, ngramCounts.getNgramCounterSize());
	}

	@Test
	public void testGetTotalWordNumber() {
		this.ngramCounts.ngramCounts.put("toto", 2);
		this.ngramCounts.ngramCounts.put("titi", 6);
		this.ngramCounts.ngramCounts.put("tata", 4);
		assertEquals(12, ngramCounts.getTotalWordNumber());
	}

	@Test
	public void testGetNgrams() {
		this.ngramCounts.ngramCounts.put("toto", 2);
		this.ngramCounts.ngramCounts.put("titi", 6);
		this.ngramCounts.ngramCounts.put("tata", 4);
		assertEquals("[toto, titi, tata]", ngramCounts.getNgrams().toString());
	}

	@Test
	public void testGetCounts() {
		this.ngramCounts.ngramCounts.put("toto", 2);
		this.ngramCounts.ngramCounts.put("titi", 6);
		this.ngramCounts.ngramCounts.put("tata", 4);
		assertEquals(2, ngramCounts.getCounts("toto"));
	}

	@Test
	public void testIncCounts() {
		this.ngramCounts.ngramCounts.put("toto", 2);
		this.ngramCounts.ngramCounts.put("titi", 6);
		this.ngramCounts.ngramCounts.put("tata", 4);
		ngramCounts.incCounts("toto");
		assertEquals(3, ngramCounts.getCounts("toto"));
	}

	@Test
	public void testSetCounts() {
		this.ngramCounts.ngramCounts.put("toto", 2);
		this.ngramCounts.ngramCounts.put("titi", 6);
		this.ngramCounts.ngramCounts.put("tata", 4);
		ngramCounts.setCounts("toto", 3);
		assertEquals(3, ngramCounts.getCounts("toto"));
	}

	@Test
	public void testScanTextFile() {
		Vocabulary vocab = new Vocabulary();
		vocab.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");
		this.ngramCounts.scanTextFile("data/small_corpus/corpus_fr_train.txt", vocab, 2);
		assertEquals(1,this.ngramCounts.getCounts("<s> denis"));
		assertEquals(2,this.ngramCounts.getCounts("écoute une"));
		assertEquals(1,this.ngramCounts.getCounts("de"));
		assertEquals(3,this.ngramCounts.getCounts("<s>"));
		assertEquals(1,this.ngramCounts.getCounts("elle écoute"));
		assertEquals(1,this.ngramCounts.getCounts("thom </s>"));
		assertEquals(1,this.ngramCounts.getCounts("lionel"));
		assertEquals(1,this.ngramCounts.getCounts("denis"));
		assertEquals(1,this.ngramCounts.getCounts("une chanson"));
		assertEquals(2,this.ngramCounts.getCounts("chanson"));
		assertEquals(1,this.ngramCounts.getCounts("<s> elle"));
		assertEquals(1,this.ngramCounts.getCounts("autre chanson"));
		assertEquals(1,this.ngramCounts.getCounts("elle"));
		assertEquals(1,this.ngramCounts.getCounts("denis écoute"));
		assertEquals(1,this.ngramCounts.getCounts("chanson </s>"));
		assertEquals(1,this.ngramCounts.getCounts("lionel </s>"));
	}

	@Test
	public void testWriteNgramCountFile() {
		Vocabulary vocab = new Vocabulary();
		vocab.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");
		this.ngramCounts.scanTextFile("data/small_corpus/corpus_fr_train.txt", vocab, 2);
		this.ngramCounts.writeNgramCountFile("lm/small_corpus/testNgramCounts_bigram_vocabulary1.txt");
	}

	@Test
	public void testReadNgramCountsFile() {
		this.ngramCounts.readNgramCountsFile("lm/small_corpus/ngramCounts_bigram_vocabulary1.txt");
	}

}
