package langModel;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class VocabularyTest {

	private Vocabulary vocabulary1 = new Vocabulary();
	private Vocabulary vocabulary2 = new Vocabulary();
	
	@Before
	public void setUp() throws Exception {
		this.vocabulary1 = new Vocabulary();
		this.vocabulary2 = new Vocabulary();
		vocabulary1.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");	
		vocabulary2.readVocabularyFile("lm/small_corpus/vocabulary2_in.txt");
	}

	@Test
	public void testVocabulary() {
		assertNotNull(this.vocabulary1.vocabulary); 
		assertNotNull(this.vocabulary2.vocabulary); 
	}

	@Test
	public void testGetSize() {
		assertEquals(12,this.vocabulary1.getSize());
		assertEquals(13,this.vocabulary2.getSize());
	}

	@Test
	public void testGetWords() {
		List<String> vocabularyTest1 = new ArrayList<String>();
		vocabularyTest1.add("<s>");
		vocabularyTest1.add("</s>");
		vocabularyTest1.add("antoine");
		vocabularyTest1.add("écoute");
		vocabularyTest1.add("thom");
		vocabularyTest1.add("denis");
		vocabularyTest1.add("une");
		vocabularyTest1.add("autre");
		vocabularyTest1.add("chanson");
		vocabularyTest1.add("elle");
		vocabularyTest1.add("de");
		vocabularyTest1.add("lionel");
		
		List<String> vocabularyTest2 = new ArrayList<String>();
		vocabularyTest2.add("<s>");
		vocabularyTest2.add("</s>");
		vocabularyTest2.add("<unk>");
		vocabularyTest2.add("antoine");
		vocabularyTest2.add("écoute");
		vocabularyTest2.add("thom");
		vocabularyTest2.add("daniel");
		vocabularyTest2.add("une");
		vocabularyTest2.add("autre");
		vocabularyTest2.add("chanson");
		vocabularyTest2.add("elle");
		vocabularyTest2.add("de");
		vocabularyTest2.add("brice");
		
		List<String> vocabulary1 = new ArrayList<String>(this.vocabulary1.getWords());
		List<String> vocabulary2 = new ArrayList<String>(this.vocabulary2.getWords());
		
		// On trie par ordre alphabétique
		Collections.sort(vocabularyTest1);
		Collections.sort(vocabularyTest2);
		Collections.sort(vocabulary1);
		Collections.sort(vocabulary2);
		
		assertEquals(vocabularyTest1.toString(),vocabulary1.toString());
		assertEquals(vocabularyTest2.toString(),vocabulary2.toString());
	}

	@Test
	public void testContains() {
	 	assertTrue(this.vocabulary1.contains("antoine"));
	 	assertTrue(this.vocabulary2.contains("antoine"));
	}

	@Test
	public void testAddWord() {
		this.vocabulary1.addWord("toto");
	 	this.vocabulary2.addWord("toto");
	 	assertTrue(this.vocabulary1.contains("toto"));
	 	assertTrue(this.vocabulary2.contains("toto"));
	}

	@Test
	public void testRemoveWord() {
		this.vocabulary1.removeWord("antoine");
	 	this.vocabulary2.removeWord("antoine");
	 	assertFalse(this.vocabulary1.contains("antoine"));
	 	assertFalse(this.vocabulary2.contains("antoine"));
	}

	@Test
	public void testScanNgramSet() {
		Set<String> vocabularyTest1 = new HashSet<String>();
		vocabularyTest1.add("<s>");
		vocabularyTest1.add("</s>");
		vocabularyTest1.add("antoine");
		vocabularyTest1.add("écoute");
		vocabularyTest1.add("thom");
		vocabularyTest1.add("denis");
		vocabularyTest1.add("une");
		vocabularyTest1.add("autre");
		vocabularyTest1.add("chanson");
		vocabularyTest1.add("elle");
		vocabularyTest1.add("de");
		vocabularyTest1.add("lionel");
		
		Set<String> vocabularyTest2 = new HashSet<String>();
		vocabularyTest2.add("<s>");
		vocabularyTest2.add("</s>");
		vocabularyTest2.add("<unk>");
		vocabularyTest2.add("antoine");
		vocabularyTest2.add("écoute");
		vocabularyTest2.add("thom");
		vocabularyTest2.add("daniel");
		vocabularyTest2.add("une");
		vocabularyTest2.add("autre");
		vocabularyTest2.add("chanson");
		vocabularyTest2.add("elle");
		vocabularyTest2.add("de");
		vocabularyTest2.add("brice");
		
		this.vocabulary1.scanNgramSet(vocabularyTest2);
		this.vocabulary2.scanNgramSet(vocabularyTest1);
		
		assertEquals(vocabularyTest2.toString(),this.vocabulary1.vocabulary.toString());
		assertEquals(vocabularyTest1.toString(),this.vocabulary2.vocabulary.toString());
	}

	@Test
	public void testReadVocabularyFile() {
		vocabulary1.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");	
		vocabulary2.readVocabularyFile("lm/small_corpus/vocabulary2_in.txt");
	}

	@Test
	public void testWriteVocabularyFile() {
		vocabulary1.writeVocabularyFile("lm/small_corpus/testVocabulary1_in.txt");	
		vocabulary2.writeVocabularyFile("lm/small_corpus/testVocabulary2_in.txt");
	}

}
