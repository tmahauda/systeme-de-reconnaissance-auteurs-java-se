package langModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NaiveLanguageModelTest {

	private NgramCountsInterface ngramCounts;
	private VocabularyInterface vocabulary;
	private NaiveLanguageModel languageModel;
	
	@Before
	public void setUp() throws Exception {
		this.languageModel = new NaiveLanguageModel();
		this.vocabulary = new Vocabulary();
		this.ngramCounts = new NgramCounts();
	}

	@Test
	public void testGetLMOrder() {
		assertEquals(this.languageModel.ngramCounts.getMaximalOrder(), this.languageModel.getLMOrder());
	}

	@Test
	public void testSetNgramCounts() {
		this.languageModel.setNgramCounts(this.ngramCounts, this.vocabulary);
		assertEquals(this.ngramCounts, this.languageModel.ngramCounts);
		assertEquals(this.vocabulary, this.languageModel.vocabulary);
	}

	@Test
	public void testGetNgramProb() {
		this.vocabulary.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");
		this.ngramCounts.scanTextFile("data/small_corpus/corpus_fr_train.txt", this.vocabulary, 2);
		this.languageModel.setNgramCounts(this.ngramCounts, this.vocabulary);
		assertEquals(0.333, this.languageModel.getNgramProb("<s> antoine"), 0.001);
	}

	@Test
	public void testGetSentenceProb() {
		String sentence = "<s> antoine écoute une chanson </s>";
		this.vocabulary.readVocabularyFile("lm/small_corpus/vocabulary1_in.txt");
		this.ngramCounts.scanTextFile("data/small_corpus/corpus_fr_train.txt", this.vocabulary, 2);
		this.languageModel.setNgramCounts(this.ngramCounts, this.vocabulary);
		assertEquals(0.021 , this.languageModel.getSentenceProb(sentence), 0.001);
	}
}
