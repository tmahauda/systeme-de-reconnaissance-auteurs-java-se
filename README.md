# Système de reconnaissance d'auteurs

<div align="center">
<img width="500" height="400" src="reconnaissance_auteur.jpeg">
</div>

## Description du projet

Application console réalisée avec Java SE en DUT INFO 2 à l'IUT de Nantes dans le cadre du module "Modélisations Mathématiques" durant l'année 2017-2018 avec un groupe de deux personnes. \
Elle permet d'identifier des auteurs de texte selon des analyses statistiques.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de trois étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Clara DEGREZ : clara.degrez@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par deux enseignants de l'IUT de Nantes :
- Solen QUINIOU : solen.quiniou@univ-nantes.fr ;
- Nicolas HERNANDEZ : nicolas.hernandez@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Modélisations Mathématiques" du DUT INFO 2.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2018 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 12/02/2018.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse.

## Objectifs

La finalité de ce projet est de pouvoir identifier automatiquement l'auteur d'un texte.
